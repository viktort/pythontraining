'''classes with string representation'''

class HelloWorld(object):
    def __init__(self):
        self.datum = 'Hello World.'

    def __str__(self):
        return self.datum

if __name__ == '__main__':
    obj = HelloWorld()
    print(obj)


