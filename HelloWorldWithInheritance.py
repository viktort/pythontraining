class StringStore(object):
    def __init__(self):
        self.datum = 'Hello World.'

    def __str__(self):
        return self.datum


class HelloWorld(StringStore):
    def __init__(self):
        super(HelloWorld, self).__init__('Hello World')


if __name__ == '__main__':
    obj = HelloWorld()
    print(obj)