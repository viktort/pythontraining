# slides for the day http://www.russel.org.uk/Workshops/Python/HolidayExtras/2015-11-23/Slides/

http://www.russel.org.uk/

http://www.amazon.co.uk/Python-Rookies-First-Course-Programming/dp/1844807010

https://www.python.org/dev/peps/pep-0008/

# import this    -> prints the zen of python

pythonic -> python writen in harmony with ZEN and python guide etc.
pythonesque -> hard to read python - it is used to describe this.

PyDoc from the Terminal -> generates the doc from a well documented module

Python3 is a shift of statements into functions from earlier versions of python.


*** ask about bundling in python. I.E. maven equivalent

declaritive reading
operational reading

__future__ -> tell the compiler to use the function from python3

from __future__ import print_function
print ('hello world') -> tells compiler to use the python3 function instead of decralative python 2x

range in python3 use xrange in python2 -> in python3 range is actually using xrange.

while vs for -> use while in the context of unbounded iteration. Use for loop for bounded iteration.
e.g. read a stream of bytes - you dont know how long the stream is so this would be unbounded iteration.


pip search pytest  -> eqivalent to apt-cache search foo. python index package

lxml if working with XML

pip ''install'' and ''virtualenv'' for packaging code

TOOLS: -> check out
functTools
iterTools

for a method in a class, param passed in is always called 'self'


