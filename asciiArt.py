def _validate(x):
    if x < 0:
        raise ValueError('Argument must be a positive integer')

    if not isinstance(x, int):
        raise TypeError('Value must be an integer')

class RectAngle(object):
    def __init__(self, height, width):
        _validate(height)
        _validate(width)
        self.height = height
        self.width = width

    def __str__(self):
        val = ''
        for i in range(self.height):
            for l in range(self.width):
                val += '* '
            val += '\n'
        return val


class Square(RectAngle):
    ''' where to use  inheritance in Python'''
    def __init__(self, size):
        _validate(size)
        super(Square, self).__init__(size, size)


if __name__ == '__main__':
    print(RectAngle(2, 4))
    print(RectAngle(5, 5))
    print(Square(4))
