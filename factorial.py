# -*- coding:utf-8;-*-

from functools import reduce
from operator import mul

def _validate(x):
    if not isinstance(x, int):
        raise TypeError('Parameter is not an int')

    if x < 0:
        raise ValueError('Argument must be a positive integer')


def factorialIterative(x):
    _validate(x)
    total = 1
    if x > 1:
        for i in range(1, x):
            total *= (x - i+1)

    return total


def recursiveCall(x):
    _validate(x)
    if x > 1:
        return x * recursiveCall(x-1)

    return 1


def tailRecursive(x):
    _validate(x)
    if x < 2:
        return 1
    else:
        def iterate(i, result=1):
            return result if i < 2 else iterate(i -1, result * i)
        return iterate(x)


def usingReduce(x):
    _validate(x)
    if x < 2:
        return 1
    return reduce(mul, range(2, x+1))


if __name__ == '__main__':
    print(factorialIterative(9))
    print(recursiveCall(9))
    print(usingReduce(9))
