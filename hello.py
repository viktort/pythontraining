#! /usr/bin/env python3
from numpy.core.umath import rad2deg
# -*-coding:utf-8-*-

print("Hello world.") #brackets are good!

data = ['hello', ' ', 'world', '.']
result = ''
for i in range(len(data)):  # This is not pythonic for s in data: ->  still not very pythonic
    result += data[i]

print(result)


print(''.join(data)) # -> pythonic

#print(''.join([1, 'foo'])) ->>>> error - join expects same type Objects


print([s.strip() for s in data]);


def helloWorld():
    print('hello world')


def helloWorld1(a, b, c, d):
    print(a + b + c + d)


def helloWorld1WithDefaults(a='hello', b=' ', c='moo', d='!'):
    print(a + b + c + d)


def helloWithMultipleParams(*args):
    print(''.join(args))


def helloWithMultipleParamsSafe(a, *args):
    print(a + ''.join(args))


def support(**kwars):
    print(kwars['a'] + kwars['b'] + kwars['c'] + kwars['d'])

print(locals())

''' -> execute it if it is called from the same file as where it is defined
    -> similar to main method/class in java -> need to bootstrap the execution
    the entire module inside this condition
'''

if __name__ == '__main__':
    helloWorld()
    helloWorld1(*['hello', ' ', 'world', '.'])  # in python3 only -> map each element in array to parameters to method
    helloWithMultipleParams('hello', ' ', 'Foo', ' ', 'Bar', '.')
    helloWithMultipleParamsSafe('hello', ' ', 'Foo', ' ', 'Bar', '.')  # checks that at least one value was passed in
    helloWorld1WithDefaults()  # or
    helloWorld1WithDefaults('hello', ' ', 'doo', '!')
    helloWorld1WithDefaults(a='ckemi', c='mo')  # keyword arguments - uses the defaults and replaces the defined ones


