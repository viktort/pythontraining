from __future__ import division
import math
import sys

# amount = sys.argv[1]
amount = input("enter amount: ")
years = 10
amount1 = amount
amount2 = amount
amount3 = amount
delimiter = '\t\t'


def percentage(val, amount):
    return val/100 * amount

print('Year' + delimiter +'3%' + delimiter + '5%' + delimiter + '7%')

for i in range(years + 1):
    amount1 += percentage(3, amount1)
    amount2 += percentage(5, amount2)
    amount3 += percentage(7, amount3)

    print(`2015 + i` + delimiter + '{:.2f}'.format(amount1) + delimiter + '{:.2f}'.format(amount2) + delimiter + '{:.2f}'.format(amount3))


#print([x for x in range(1, years) if x * x % years == 0])
#print([s.strip() for s in aList])