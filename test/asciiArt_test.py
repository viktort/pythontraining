import unittest
from asciiArt import Square
from asciiArt import RectAngle


class TestAsciiArt(unittest.TestCase):
    def test_square_with_minus_one(self):
        self.assertRaises(ValueError, Square, -1)

    def test_square_with_str(self):
        self.assertRaises(TypeError, Square, 'str')

    def test_rectangle_with_minus_one(self):
        self.assertRaises(ValueError, RectAngle, -1, 2)

    def test_rectangle_with_minus_one_second_param(self):
        self.assertRaises(ValueError, RectAngle, 1, -1)

    def test_rectangle_with_minus_one_both_param(self):
        self.assertRaises(ValueError, RectAngle, -5, -1)

    def test_rectangle_with_str_both_param(self):
        self.assertRaises(TypeError, RectAngle, 'foo', 'bar')

    def test_rectangle_with_str_first_param(self):
        self.assertRaises(TypeError, RectAngle, 'foo', 3)

    def test_rectangle_with_str_second_param(self):
        self.assertRaises(TypeError, RectAngle, 3, 'foo')

if __name__ == '__main__':
    unittest.main()