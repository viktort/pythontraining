import unittest
from factorial import factorialIterative
from factorial import recursiveCall
from factorial import usingReduce


class TestFactorial(unittest.TestCase):
    def test_iterative_zero(self):
        self.assertEqual(1, factorialIterative(0))

    def test_iterative_five(self):
        self.assertEqual(120, factorialIterative(5))

    def test_recursive_zero(self):
        self.assertEqual(1, recursiveCall(0))

    def test_recursive_five(self):
        self.assertEqual(120, recursiveCall(5))

    def test_recursive_minus_one(self):
        self.assertRaises(ValueError, recursiveCall, -1)

    def test_usingReduce_zero(self):
        self.assertEqual(1, usingReduce(0))

    def test_usingReduce_five(self):
        self.assertEqual(120, usingReduce(5))

    def test_usingReduce_minus_one(self):
        self.assertRaises(ValueError, usingReduce, -1)

    def test_usingReduce_string(self):
        self.assertRaises(TypeError, usingReduce, 'str')

if __name__ == '__main__':
    unittest.main()
